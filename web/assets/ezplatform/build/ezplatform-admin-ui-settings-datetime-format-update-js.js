(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ezplatform-admin-ui-settings-datetime-format-update-js"],{

/***/ "./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js":
/*!*****************************************************************************************************************************!*\
  !*** ./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("(function (global, doc, moment) {\n  var SELECTOR_DATE_FORMAT = '#user_setting_update_value_date_format';\n  var SELECTOR_TIME_FORMAT = '#user_setting_update_value_time_format';\n  var SELECTOR_VALUE_PREVIEW = '.ez-datetime-format-preview-value';\n  var valuePreview = doc.querySelector(SELECTOR_VALUE_PREVIEW);\n  var dateFormatSelect = doc.querySelector(SELECTOR_DATE_FORMAT);\n  var timeFormatSelect = doc.querySelector(SELECTOR_TIME_FORMAT);\n\n  var updateDateTimeFormatPreview = function updateDateTimeFormatPreview() {\n    valuePreview.innerHTML = moment().formatICU(dateFormatSelect.value + ' ' + timeFormatSelect.value);\n  };\n\n  dateFormatSelect.addEventListener('change', updateDateTimeFormatPreview);\n  timeFormatSelect.addEventListener('change', updateDateTimeFormatPreview);\n  updateDateTimeFormatPreview();\n})(window, window.document, window.moment);\n\n//# sourceURL=webpack:///./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js?");

/***/ }),

/***/ 23:
/*!***********************************************************************************************************************************!*\
  !*** multi ./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! /Users/sd/Projects/ez/ezplatform-custom-attributes-example/vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js */\"./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js\");\n\n\n//# sourceURL=webpack:///multi_./vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/scripts/admin.settings.datetimeformat.update.js?");

/***/ })

},[[23,"runtime"]]]);