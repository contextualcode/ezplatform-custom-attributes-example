(function (t) {
// en
t.add("anchor_btn.error.unique", "This anchor already exists on the page. Anchor name must be unique.", "alloy_editor", "en");
t.add("anchor_btn.label", "Anchor", "alloy_editor", "en");
t.add("anchor_edit.btn.remove.title", "Remove", "alloy_editor", "en");
t.add("anchor_edit.btn.save.title", "Save", "alloy_editor", "en");
t.add("anchor_edit.input.label", "Name:", "alloy_editor", "en");
t.add("block_text_align_center_btn.label", "Center", "alloy_editor", "en");
t.add("blocktext_align_justify_btn.label", "Justify", "alloy_editor", "en");
t.add("blocktext_align_left_btn.label", "Left", "alloy_editor", "en");
t.add("blocktext_align_right_btn.label", "Right", "alloy_editor", "en");
t.add("custom_tag_update_btn.save_btn.label", "Save", "alloy_editor", "en");
t.add("embed_align_center_btn.label", "Center", "alloy_editor", "en");
t.add("embed_align_left_btn.label", "Left", "alloy_editor", "en");
t.add("embed_align_right_btn.label", "Right", "alloy_editor", "en");
t.add("embed_btn.label", "Embed", "alloy_editor", "en");
t.add("embed_btn.udw.title", "Select a content to embed", "alloy_editor", "en");
t.add("embed_update_btn.label", "Select another content item", "alloy_editor", "en");
t.add("embed_update_btn.udw.title", "Select a content to embed", "alloy_editor", "en");
t.add("heading_btn.label", "Heading", "alloy_editor", "en");
t.add("image_btn.label", "Image", "alloy_editor", "en");
t.add("image_btn.udw.label", "Select an image to embed", "alloy_editor", "en");
t.add("image_update_btn.label", "Select another image item", "alloy_editor", "en");
t.add("image_update_btn.udw.title", "Select an image to embed", "alloy_editor", "en");
t.add("link_edit_btn.action_row.remove", "Remove", "alloy_editor", "en");
t.add("link_edit_btn.action_row.save", "Save", "alloy_editor", "en");
t.add("link_edit_btn.button_row.block.placeholder.text", "Type or paste link here", "alloy_editor", "en");
t.add("link_edit_btn.button_row.link_to", "Link to:", "alloy_editor", "en");
t.add("link_edit_btn.button_row.select", "Select:", "alloy_editor", "en");
t.add("link_edit_btn.button_row.select_content", "Select content", "alloy_editor", "en");
t.add("link_edit_btn.button_row.separator", "or", "alloy_editor", "en");
t.add("link_edit_btn.info_row.new_tab", "New tab", "alloy_editor", "en");
t.add("link_edit_btn.info_row.open_in.label", "Open in:", "alloy_editor", "en");
t.add("link_edit_btn.info_row.same_tab", "Same tab", "alloy_editor", "en");
t.add("link_edit_btn.info_row.title", "Title:", "alloy_editor", "en");
t.add("link_edit_btn.udw.title", "Select content", "alloy_editor", "en");
t.add("move_down_btn.title", "Move Down", "alloy_editor", "en");
t.add("move_up_btn.title", "Move Up", "alloy_editor", "en");
t.add("ordered_list_btn.label", "List", "alloy_editor", "en");
t.add("paragraph_btn.label", "Paragraph", "alloy_editor", "en");
t.add("remove_block_btn.title", "Remove block", "alloy_editor", "en");
t.add("table_btn.label", "Table", "alloy_editor", "en");
t.add("toolbar_config_base.formatted_label", "Formatted", "alloy_editor", "en");
t.add("toolbar_config_base.heading_label", "Heading", "alloy_editor", "en");
t.add("toolbar_config_base.paragraph_label", "Paragraph", "alloy_editor", "en");
t.add("unordered_list_btn.label", "List", "alloy_editor", "en");
t.add("content.create_draft.success", "New Version Draft for '%name%' created.", "content", "en");
t.add("content.draft.conflict.error", "Cannot check if the draft has no conflict with other drafts. %error%. ", "content", "en");
t.add("content.edit.permission.error", "You don't have permission to edit the content", "content", "en");
t.add("content.hide.already_hidden", "Content '%name%' has been hidden.", "content", "en");
t.add("content.hide.success", "Content '%name%' has been hidden.", "content", "en");
t.add("content.main_language_update.success", "Main language for '%name%' updated.", "content", "en");
t.add("content.main_location_update.success", "Main location for '%name%' updated.", "content", "en");
t.add("content.reveal.already_visible", "Content '%name%' was already visible.", "content", "en");
t.add("content.reveal.success", "Content '%name%' has been revealed.", "content", "en");
t.add("user.delete.success", "User with login '%login%' deleted.", "content", "en");
t.add("ezimageasset.create.message.error", "Error while creating image asset: %error%", "fieldtypes_edit", "en");
t.add("ezimageasset.create.message.success", "Image has been published and can now be reused", "fieldtypes_edit", "en");
t.add("bookmark.list.empty", "The notification list is empty.", "notifications", "en");
t.add("menu.notification", "View Notifications", "notifications", "en");
t.add("notification.date", "Date", "notifications", "en");
t.add("notification.description", "Description", "notifications", "en");
t.add("notification.no_longer_available", "Content not available any more", "notifications", "en");
t.add("notification.permanently_deleted", "Publication permanently deleted", "notifications", "en");
t.add("notification.title", "Title:", "notifications", "en");
t.add("notification.trashed", "Publication sent to trash", "notifications", "en");
t.add("notification.type", "Type", "notifications", "en");
t.add("notifications", "Notifications", "notifications", "en");
t.add("notifications.modal.message.error", "Cannot update notifications count", "notifications", "en");
t.add("modal.cancel", "Cancel", "search", "en");
t.add("modal.select", "Select", "search", "en");
t.add("search", "Search", "search", "en");
t.add("search.any.content.type", "Any content type", "search", "en");
t.add("search.apply", "Apply", "search", "en");
t.add("search.clear", "Clear", "search", "en");
t.add("search.content.type", "Content Type:", "search", "en");
t.add("search.created", "Created:", "search", "en");
t.add("search.creator", "Creator:", "search", "en");
t.add("search.creator_input.placeholder", "Type creator", "search", "en");
t.add("search.custom_range", "Custom range", "search", "en");
t.add("search.date.from", "From:", "search", "en");
t.add("search.date.range", "Select date range", "search", "en");
t.add("search.date.to", "To:", "search", "en");
t.add("search.filters", "Filters", "search", "en");
t.add("search.header", "Search results (%total%)", "search", "en");
t.add("search.headline", "Search", "search", "en");
t.add("search.last.modified", "Last modified:", "search", "en");
t.add("search.last_month", "Last month", "search", "en");
t.add("search.last_week", "Last week", "search", "en");
t.add("search.last_year", "Last year", "search", "en");
t.add("search.list", "search.list", "search", "en");
t.add("search.modified", "Modified", "search", "en");
t.add("search.name", "Name", "search", "en");
t.add("search.no_result", "Sorry, no results were found for \"%query%\".", "search", "en");
t.add("search.perform", "Search", "search", "en");
t.add("search.section", "Section:", "search", "en");
t.add("search.select_content", "Select Content", "search", "en");
t.add("search.subtree", "Subtree:", "search", "en");
t.add("search.tips.check_spelling", "Check spelling of keywords.", "search", "en");
t.add("search.tips.different_keywords", "Try different keywords.", "search", "en");
t.add("search.tips.fewer_keywords", "Try fewer keywords. Reducing keywords result in more matches.", "search", "en");
t.add("search.tips.headline", "Some helpful search tips:", "search", "en");
t.add("search.tips.more_general_keywords", "Try more general keywords.", "search", "en");
t.add("search.type", "Content Type", "search", "en");
t.add("search.udw.select_content", "Select Content", "search", "en");
t.add("search.viewing", "Viewing %viewing% out of %total% sub-items", "search", "en");
t.add("add_location.title", "Select location", "universal_discovery_widget", "en");
t.add("browse.title", "Browse content", "universal_discovery_widget", "en");
t.add("copy.title", "Select location", "universal_discovery_widget", "en");
t.add("dashboard.create.title", "Create your content", "universal_discovery_widget", "en");
t.add("ezimageasset.title", "Select Image Asset", "universal_discovery_widget", "en");
t.add("ezobjectrelationlist.title", "Select content", "universal_discovery_widget", "en");
t.add("limitation.pick.error", "Failed to fetch content names", "universal_discovery_widget", "en");
t.add("move.title", "Select destination", "universal_discovery_widget", "en");
t.add("restore_under_new_location.title", "Select a location to restore you content item(s)", "universal_discovery_widget", "en");
t.add("select_location.error", "Cannot find children locations with given id - %idList%", "universal_discovery_widget", "en");
t.add("subtree.title", "Select location", "universal_discovery_widget", "en");
t.add("subtree_limitation.title", "Choose locations", "universal_discovery_widget", "en");
t.add("swap.title", "Select location to be swapped with", "universal_discovery_widget", "en");
t.add("bookmarks.tab.label", "Bookmarks", "universal_discovery_widget", "en");
t.add("bookmarks_table.no_bookmarks.message", "No content items. Content items you bookmark will appear here.", "universal_discovery_widget", "en");
t.add("bookmarks_table.title", "Bookmarks", "universal_discovery_widget", "en");
t.add("browse.tab.label", "Browse", "universal_discovery_widget", "en");
t.add("cancel.label", "Cancel", "universal_discovery_widget", "en");
t.add("confirm.label", "Confirm", "universal_discovery_widget", "en");
t.add("content_meta_preview.creation_date.label", "Creation date", "universal_discovery_widget", "en");
t.add("content_meta_preview.image_preview_not_available.info", "Content preview is not available", "universal_discovery_widget", "en");
t.add("content_meta_preview.last_modified.label", "Last modified", "universal_discovery_widget", "en");
t.add("content_meta_preview.title", "Content Meta Preview", "universal_discovery_widget", "en");
t.add("content_meta_preview.translations.label", "Translations", "universal_discovery_widget", "en");
t.add("content_on_the_fly.choose_language_and_content_type.title", "Choose Language and Content Type", "universal_discovery_widget", "en");
t.add("content_on_the_fly.create_content.label", "Create content", "universal_discovery_widget", "en");
t.add("content_on_the_fly.creating_content.title", "Creating - %contentType% in %language%", "universal_discovery_widget", "en");
t.add("content_on_the_fly.location_not_allowed.message", "Sorry, but this location is not selectable.", "universal_discovery_widget", "en");
t.add("content_on_the_fly.no_permission.message", "Sorry, but you don't have permission for this action. Please contact your site Admin.", "universal_discovery_widget", "en");
t.add("content_on_the_fly.publish.label", "Publish", "universal_discovery_widget", "en");
t.add("content_on_the_fly.select_a_content_type.title", "Select a Content Type", "universal_discovery_widget", "en");
t.add("content_on_the_fly.select_language.title", "Select a language", "universal_discovery_widget", "en");
t.add("content_on_the_fly.select_location.title", "Select Location", "universal_discovery_widget", "en");
t.add("content_on_the_fly.type_to_refine.placeholder", "Type to refine", "universal_discovery_widget", "en");
t.add("content_table.header.name", "Name", "universal_discovery_widget", "en");
t.add("content_table.header.type", "Content Type", "universal_discovery_widget", "en");
t.add("content_table.not_available.label", "N\/A", "universal_discovery_widget", "en");
t.add("create.tab.label", "Create", "universal_discovery_widget", "en");
t.add("finder.branch.load_more.label", "Load more", "universal_discovery_widget", "en");
t.add("pagination.first", "First", "universal_discovery_widget", "en");
t.add("pagination.last", "Last", "universal_discovery_widget", "en");
t.add("pagination.next", "Next", "universal_discovery_widget", "en");
t.add("pagination.prev", "Previous", "universal_discovery_widget", "en");
t.add("popup.close.label", "Close", "universal_discovery_widget", "en");
t.add("search.content_table.title", "Search results", "universal_discovery_widget", "en");
t.add("search.results_table.header.name", "Name", "universal_discovery_widget", "en");
t.add("search.results_table.header.type", "Content Type", "universal_discovery_widget", "en");
t.add("search.results_table.not_available.label", "N\/A", "universal_discovery_widget", "en");
t.add("search.results_table.title", "Search results", "universal_discovery_widget", "en");
t.add("search.submit.label", "Search", "universal_discovery_widget", "en");
t.add("search.tab.label", "Search", "universal_discovery_widget", "en");
t.add("search.title", "Search", "universal_discovery_widget", "en");
t.add("select_content.confirmed_items.title", "Confirmed items", "universal_discovery_widget", "en");
t.add("select_content.limit.label", "Limit %items% max", "universal_discovery_widget", "en");
t.add("select_content.no_confirmed_content.title", "No confirmed content yet", "universal_discovery_widget", "en");
t.add("select_content.not_available.label", "N\/A", "universal_discovery_widget", "en");
t.add("collapse_all", "Collapse all", "content_tree", "en");
t.add("expand_item.limit.message", "Cannot load subitems for given location due to performance limits. You reached max tree depth.", "content_tree", "en");
t.add("loading_more", "Loading more...", "content_tree", "en");
t.add("no_subitems", "This location has no sub-items", "content_tree", "en");
t.add("show_more", "Show more", "content_tree", "en");
t.add("show_more.limit_reached", "Loading limit reached", "content_tree", "en");
t.add("abort.label", "Abort", "multi_file_upload", "en");
t.add("cannot_upload.message", "Cannot upload file", "multi_file_upload", "en");
t.add("delete.label", "Delete", "multi_file_upload", "en");
t.add("disallowed_content_type.message", "You do not have permission to create this content item", "multi_file_upload", "en");
t.add("disallowed_size.message", "File size is not allowed", "multi_file_upload", "en");
t.add("disallowed_type.message", "File type is not allowed", "multi_file_upload", "en");
t.add("drop_action.message", "Drag and drop your files on browser window or upload them", "multi_file_upload", "en");
t.add("edit.label", "Edit", "multi_file_upload", "en");
t.add("max_file_size.message", "Max file size:", "multi_file_upload", "en");
t.add("multi_file_upload_open_btn.label", "Upload sub-items", "multi_file_upload", "en");
t.add("upload.success.message", "Uploaded", "multi_file_upload", "en");
t.add("upload_btn.label", "Upload sub-items", "multi_file_upload", "en");
t.add("upload_popup.title", "Multi-file upload", "multi_file_upload", "en");
t.add("bulk_delete.error.message", "%notDeletedCount% of the %totalCount% selected item(s) could not be deleted because you do not have proper user permissions. {{ moreInformationLink }} Please contact your Administrator to obtain permissions.", "sub_items", "en");
t.add("bulk_delete.error.modal.table_title", "Content item(s) cannot be sent to trash (%itemsCount%)", "sub_items", "en");
t.add("bulk_delete.error.more_info", "<u><a class='ez-notification-btn ez-notification-btn--show-modal'>Click here for more information.<\/a><\/u><br>", "sub_items", "en");
t.add("bulk_delete.popup.cancel", "Cancel", "sub_items", "en");
t.add("bulk_delete.popup.confirm", "Send to trash", "sub_items", "en");
t.add("bulk_delete.popup.message", "Are you sure you want to send the selected content item(s) to trash?", "sub_items", "en");
t.add("bulk_delete.success.message", "The selected content item(s) have been sent to trash", "sub_items", "en");
t.add("bulk_move.error.message", "%notMovedCount% of the %totalCount% selected item(s) could not be moved because you do not have proper user permissions. {{ moreInformationLink }} Please contact your Administrator to obtain permissions.", "sub_items", "en");
t.add("bulk_move.error.modal.table_title", "Content items cannot be moved (%itemsCount%)", "sub_items", "en");
t.add("bulk_move.error.more_info", "<u><a class='ez-notification-btn ez-notification-btn--show-modal'>Click here for more information.<\/a><\/u><br>", "sub_items", "en");
t.add("bulk_move.success.link_to_location", "<u><a href='%locationHref%'>%locationName%<\/a><\/u>", "sub_items", "en");
t.add("bulk_move.success.message", "The selected content item(s) have been sent to {{ locationLink }}", "sub_items", "en");
t.add("bulk_request.error.message", "An unexpected error occurred while deleting the content item(s). Please try again later.", "sub_items", "en");
t.add("content_type.not_available.label", "N\/A", "sub_items", "en");
t.add("edit_item_btn.label", "Edit", "sub_items", "en");
t.add("items_list.title", "Sub-items", "sub_items", "en");
t.add("items_table.header.content_type", "Content type", "sub_items", "en");
t.add("items_table.header.modified", "Modified", "sub_items", "en");
t.add("items_table.header.name", "Name", "sub_items", "en");
t.add("items_table.header.priority", "Priority", "sub_items", "en");
t.add("items_table.header.translations", "Translations", "sub_items", "en");
t.add("load_content_items.invalid_response_format.error.message", "Invalid response format", "sub_items", "en");
t.add("move_btn.label", "Move selected items", "sub_items", "en");
t.add("no_items.message", "This location has no sub-items", "sub_items", "en");
t.add("page.loading_error.message", "An error occurred while loading items in the Sub Items module", "sub_items", "en");
t.add("pagination.back", "Back", "sub_items", "en");
t.add("pagination.next", "Next", "sub_items", "en");
t.add("switch_to_grid_view.btn.label", "View as grid", "sub_items", "en");
t.add("switch_to_list_view.btn.label", "View as list", "sub_items", "en");
t.add("trash_btn.label", "Delete selected items", "sub_items", "en");
t.add("udw.choose_location.title", "Choose location", "sub_items", "en");
t.add("viewing_message", "Viewing <strong>%viewingCount%<\/strong> out of <strong>%totalCount%<\/strong> sub-items", "sub_items", "en");
t.add("options.p-class-1.label", "Paragraph Class 1", "custom_attributes", "en");
t.add("options.p-class-2.label", "Paragraph Class 2", "custom_attributes", "en");
t.add("options.p-class-3.label", "Paragraph Class 3", "custom_attributes", "en");
t.add("options.table-class-1.label", "Table Class 1", "custom_attributes", "en");
t.add("options.table-class-2.label", "Table Class 2", "custom_attributes", "en");
t.add("attributes.hide.title", "", "custom_attributes", "en");
t.add("attributes.hide.label", "Hide", "custom_attributes", "en");
t.add("attributes.max-length.title", "", "custom_attributes", "en");
t.add("attributes.max-length.label", "Maximal Length", "custom_attributes", "en");
t.add("attributes.language.title", "", "custom_attributes", "en");
t.add("attributes.language.label", "Language", "custom_attributes", "en");
t.add("options.javascript.label", "JavaScript", "custom_attributes", "en");
t.add("options.php.label", "PHP", "custom_attributes", "en");
t.add("options.java.label", "Java", "custom_attributes", "en");
t.add("embed.view.list", "List", "custom_attributes", "en");
t.add("embed.view.block", "Block", "custom_attributes", "en");
t.add("embed.view.custom-1", "Custom 1", "custom_attributes", "en");
t.add("embed.view.custom-2", "Custom 2", "custom_attributes", "en");
t.add("elements.custom_tag.label", "Custom Tag", "custom_attributes", "en");
t.add("elements.custom_style.label", "Custom Style", "custom_attributes", "en");
t.add("elements.embed.label", "Embed", "custom_attributes", "en");
t.add("elements.image.label", "Image", "custom_attributes", "en");
t.add("elements.p.label", "Paragraph", "custom_attributes", "en");
t.add("elements.h1.label", "Heading 1", "custom_attributes", "en");
t.add("elements.h2.label", "Heading 2", "custom_attributes", "en");
t.add("elements.h3.label", "Heading 3", "custom_attributes", "en");
t.add("elements.h4.label", "Heading 4", "custom_attributes", "en");
t.add("elements.h5.label", "Heading 5", "custom_attributes", "en");
t.add("elements.h6.label", "Heading 6", "custom_attributes", "en");
t.add("elements.pre.label", "Formatted", "custom_attributes", "en");
t.add("elements.ul.label", "Unordered List", "custom_attributes", "en");
t.add("elements.ol.label", "Ordered List", "custom_attributes", "en");
t.add("elements.li.label", "List Item", "custom_attributes", "en");
t.add("elements.table.label", "Table", "custom_attributes", "en");
t.add("elements.tr.label", "Row", "custom_attributes", "en");
t.add("elements.th.label", "Header", "custom_attributes", "en");
t.add("elements.td.label", "Column", "custom_attributes", "en");
t.add("elements.strong.label", "Strong", "custom_attributes", "en");
t.add("elements.em.label", "Emphasized", "custom_attributes", "en");
t.add("elements.u.label", "Underlined", "custom_attributes", "en");
t.add("elements.sub.label", "Subscript", "custom_attributes", "en");
t.add("elements.sup.label", "Superscript", "custom_attributes", "en");
t.add("elements.blockquote.label", "Quoted", "custom_attributes", "en");
t.add("elements.s.label", "Strikethrough", "custom_attributes", "en");
t.add("elements.a.label", "Link", "custom_attributes", "en");
t.add("header", "Custom Attributes for \"%tag%\"", "custom_attributes", "en");
t.add("custom_style.label", "Custom Style", "custom_attributes", "en");
t.add("custom_style.title", "", "custom_attributes", "en");
t.add("options.empty.label", "- Nothing is selected -", "custom_attributes", "en");
t.add("embed.view.title", "Embed view \"%view%\"", "custom_attributes", "en");
t.add("embed.view.embed", "Embed", "custom_attributes", "en");
})(Translator);
