<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\Yaml\Yaml;

class AppExtension extends Extension implements PrependExtensionInterface
{
    /**
     *{@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container): void
    {
        $this->prependExtension($container, 'ezrichtext_extension', 'custom_attributes');
        $this->prependExtension($container, 'ezpublish', 'xsl_stylesheets');
        $this->prependExtension($container, 'ezpublish', 'content_views');
        $this->prependExtension($container, 'ezrichtext', 'custom_tags');
        $this->prependExtension($container, 'ezpublish', 'enabled_custom_tags');
    }

    /**
     * @param ContainerBuilder $container
     * @param string $extension
     * @param strong $configFileName
     */
    protected function prependExtension(
        ContainerBuilder $container,
        string $extension,
        string $configFileName
    ): void {
        $configFile = __DIR__ . '/../Resources/config/' . $configFileName . '.yml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig($extension, $config);
        $container->addResource(new FileResource($configFile));
    }
}