<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:docbook="http://docbook.org/ns/docbook"
        exclude-result-prefixes="docbook"
        version="1.0">
  <xsl:output indent="yes" encoding="UTF-8"/>
  <xsl:variable name="outputNamespace" select="''"/>

  <xsl:template match="docbook:programlisting">
    <xsl:element name="pre" namespace="{$outputNamespace}">
      <xsl:if test="./docbook:ezattribute/docbook:ezvalue[@key='language']">
        <xsl:attribute name="data-language">
          <xsl:value-of select="./docbook:ezattribute/docbook:ezvalue[@key='language']/text()"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:value-of select="./text()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>