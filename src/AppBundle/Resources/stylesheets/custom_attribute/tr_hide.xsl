<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:docbook="http://docbook.org/ns/docbook"
        exclude-result-prefixes="docbook"
        version="1.0">
  <xsl:output indent="yes" encoding="UTF-8"/>

  <xsl:template match="docbook:tr[docbook:ezattribute[docbook:ezvalue[@key='hide' and text()='true']]]"/>

</xsl:stylesheet>