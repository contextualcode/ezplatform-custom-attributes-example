<?php

namespace AppBundle\RichText\Converter\CustomAttributes;

use DOMDocument;
use DOMXPath;
use EzSystems\EzPlatformRichText\eZ\RichText\Converter;

class MaxLength implements Converter
{
    /**
     * Handles max-length custom attribute
     *
     * @param DOMDocument $document
     *
     * @return DOMDocument
     */
    public function convert(DOMDocument $document): DOMDocument
    {
        $dataAttr = 'data-ezattribute-max-length';

        $xpath = new DOMXPath($document);
        $elements = $xpath->query('//p[@' . $dataAttr . ']');
        foreach ($elements as $element) {
            $maxLength = (int) $element->getAttribute($dataAttr);
            if ($maxLength === 0) {
                continue;
            }

            $text = $element->nodeValue;
            if (strlen($text) > $maxLength) {
                $element->nodeValue = substr($text, 0, $maxLength) . ' ...';
            }
        }

        return $document;
    }
}